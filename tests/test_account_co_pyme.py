# This file is part of the sale_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountCoPymeTestCase(ModuleTestCase):
    'Test Purchase Payment module'
    module = 'account_co_pyme'


del ModuleTestCase
